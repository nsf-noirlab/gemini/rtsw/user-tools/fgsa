import Queue
import threading
import time
import sys
import thread
from numpy import array
import numpy as np
from epics import PV
#from epics.wx import EpicsFunction, DelayedEpicsCallback
from FGSAChannelList import FGSAChannelList

global doExit
global cl
pvdata = {}

class fgsaStore(threading.Thread):

    def __init__(self, threadID, nname, q):
        self.threadID = threadID
        #self.name = name
        self.q  = q
        super(fgsaStore, self).__init__()

    def run(self):
        print ("Starting %s ") % self.name
        process_data(self.name, self.q)
        print ("Exiting %s ") + self.name

def process_data(tname, q):
    global doExit

    while not doExit:       
        queueLock.acquire()
        if not workerQueue.empty():
            data = q.get()
            write_data(data)
            queueLock.release()
        else:
            queueLock.release()

def write_data(newdata):
        
    #buff = ["data"]
    #for yval in newdata:
    #    buff.append("%f"%yval)

    datafile = open("./test3.dat", 'ab', 16384)
    if not datafile:
        print ("Error open file for writing. Exit...")
        return
    #datafile.write(buff)
    #datafile.close()
    newdata.tofile(datafile)

def setChannels():
        
    #cl = self.cl
    cl.addChannel('m2:xTipPosHS', 'X-Tip [arcsec]', 'M2 X-axis Position (Elevation)')
    cl.addChannel('m2:yTiltPosHS', 'Y-Tilt [arcsec]', 'M2 Y-axis Position (Azimuth)')
    cl.addChannel('m2:zPosHS', 'Z Focus [micron]', 'M2 Z-axis Position (Focus)')
    cl.addChannel('m2:xTipNetGuideHS', 'X-Guide [arcsec]', 'X Applied Guide (Elevation)')
    cl.addChannel('m2:yTiltNetGuideHS', 'Y-Guide [arcsec]', 'Y Applied Guide (Azimuth)')
    cl.addChannel('m2:zNetGuideHS', 'Z-Guide [micron]', 'Z Applied Guide (Focus)')
    cl.addChannel('m2:vtkXCommand', 'VTK-Xcmd [arcsec]', 'VTK X-Command (Elevation)')
    cl.addChannel('m2:vtkXFrequency', 'VTK-XFrequency [Hz]', 'VTK X-Frequency (Elevation)')
    cl.addChannel('m2:vtkXPhase', 'VTK-XPhase [degrees]', 'VTK X-Phase (Elevation)')
    cl.addChannel('m2:vtkYCommand', 'VTK-Ycmd [arcsec]', 'VTK Y-Command (Azimuth)')
    cl.addChannel('m2:vtkYFrequency', 'VTK-YFrequency [Hz]', 'VTK Y-Frequency (Azimuth)')
    cl.addChannel('m2:vtkYPhase', 'VTK-YPhase [degrees]', 'VTK Y-Phase (Azimuth)')
    cl.addChannel('m2:xRawGuideHS', 'XRawGuide [arcsec]', 'X Raw Guide (Elevation)')
    cl.addChannel('m2:yRawGuideHS', 'YRawGuide [arcsec]', 'Y Raw Guide (Azimuth)')
    cl.addChannel('m2:zRawGuideHS', 'ZRawGuide [micron]', 'Z Raw Guide (Focus)')

#@EpicsFunction
def connectChannels(channel=None):

    if channel is None:
        cname = str(cl.nameAt(0))
        #cid = 0
    #else:
        #cid = channel[0]
        #cid = 0
        #cname = channel[1]

    print 'connect newchannel %s, label=%s\n' % (channel.name,  channel.ylabel )
    #X-Position
    pv = PV(channel.name, callback=onChanges, auto_monitor = True)
    pv.get()
   
    #eventID = ca.create_subscription(chid, callback=self.onChanges)
    if pv is not None:
        if not pv.connected:
            pv.wait_for_connection()
        conn = pv.connected

        msg = 'PV not found: %s' % channel.name
        if conn:
            msg = 'PV found: %s\n' % channel.name
            pvactive = channel.name
        #self.pvmsg.SetLabel(msg)
        sys.stdout.write(msg)
        pvdata[channel.name] = [(time.time(), pv.get())]

#@DelayedEpicsCallback
# define a callback function.  Note that this should
# expect certain keyword arguments, including 'pvname' and 'value'
def onChanges(mypvname=None, value=None, **kw):
    #fmt = 'New Value: %s  value=%s, kw=%s\n'
    #sys.stdout.write(fmt % (mypvname, str(value), repr(kw)))
    #sys.stdout.flush()
    #fmt = 'New Value: %d\n'
    #sys.stdout.write(fmt % ( len(value) ))
    #sys.stdout.flush()
    #ydat = array([i[0] for i in value])
    #tnow = time.time()
    #tdat = 0.005 * (array([i[0] for i in value]) + tnow)
    ts = time.time()

    pvdata[mypvname] = [(ts,array(value))]
    global activecname
    global updateData
    activecname = mypvname
    updateData = True
    #self.onUpdatePlot()


def runtest():

    global cl
    global queueLock 
    global workerQueue
    global activecname
    global updateData
    global doExit


    doExit = False
    #updateData = False
    updateData = True
    cl = FGSAChannelList()
    setChannels()
    newchannel = cl.channelAt(0)
    connectChannels(newchannel)
    print ("init starting...\n")
    time.sleep(.1)
    print ("continuing...\n")

    queueLock = threading.Lock()
    workerQueue = Queue.Queue(10)
    fs = fgsaStore(int(1), 'test1', workerQueue)
    fs.start()
    dt = np.dtype([('timestamp', '<f8'), ('data', '<f4', (4000,))])
    #data = np.zeros((2,),dtype=dt)
    count = 20
    simulate = False

    while count > 0: 
        #count -= 1
        #time.sleep(.1)

        if updateData is not True:
            continue

        print(".")
        queueLock.acquire()
        if simulate:
            noise = np.random.normal
            srate = 200.0
            t  = np.linspace(0,20,4000)
            #y1 = np.sin(2*np.pi*12.1*t)
            y1 = 0.1*np.sin(2*np.pi*12.1*t) + noise(size=len(t), scale=0.01)
            #y1 = np.sin(x/3.4)/(0.2*x+2) + noise(size=n, scale=0.1)
            #y2 = 92 + 65*np.cos(x/16.) * np.exp(-x*x/7e3) + noise(size=n, scale=0.3)
            mydata = np.array([(time.time(), np.float32(y1))],dtype = dt)
        else:
            mydata = np.array([(time.time(), np.float32(pvdata[activecname][0][1]))],dtype=dt)
            updateData = False
            count -= 1
        
        workerQueue.put(np.array(mydata))
        queueLock.release()

    while not workerQueue.empty():
        pass

    doExit = True


    print ("Hello world!")
    

                
            
                
