# Welcome to The Fast Guide Spectrum Analyzer

The FGSA is a forensic tool intended to analyze the quality of the telescope guide corrections. This can be helpful in detecting, characterizing and supressing undesired telescope vibrations. 

![FGSA in action](./images/FGSA.png)


# Quick Introduction to the FGSA
Here's a very crude screen recording showing how the FGSA is used to monitor and verify our vibration suppression scheme (VTK) at Gemini North. The recording shows two FGSA screens side by side to analyze the autoguider. N.B. The recording will quickly shift from the FGSA desktop, over to an alternate desktop where I'm configuring the telescope and enabling a VTK algorithm. Then I quickly switch back to the FGSA desktop to monitor the performance. 

In the demo I use the left side fgsa to measure a) the raw time domain guide signal (contaminated with 12 Hz vibration signal because of GNIRS camera coolers are shaking the M1 mirror), and b) the FFT of that signal showing the 12Hz vibration which is to be suppressed. On the right side fgsa I show the VTK commanded signal ( a manufactured sin wave/phasor) designed to increase its amplitude from zero arc-seconds to track and match the incoming nefarious vibration signal and suppress it. You see the amplitude of the ~12 hz bin increase on the VTK commanded signal as the incoming 12 Hz autoguider is suppressed.

[Suppressing GNIRS Vibrations at Gemini with VTK](https://www.youtube.com/watch?v=5_QgB1o3r9M)


# Download and run FGSA
The FGSA depends on EPICS and was orininally written with Python2. The initial code imported here has been converted to Python3. Run it with `python3.9`


## Starting from Rocky8 Linux Distribution

There is a docker container image available with many (but not all) dependencies.
Start with this [EPICS 7 Container](https://gitlab.com/nsf-noirlab/gemini/rtsw/epics-base/epics-base/-/wikis/QuickStart-Docker-Setup-for-EPICS-7-and-RTEMS5)


Once you have an EPICS 7 Container, you need to add the Python3.9 dependencies.

```
$ sudo dnf install gtk3-devel python39
$ pip3.9 -v install wxpython
$ pip3.9 -v install pyepics
$ pip3.9 -v install wxmplot

$ python3.9 fgsa.py
```




