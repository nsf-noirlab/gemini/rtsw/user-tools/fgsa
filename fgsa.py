#!/usr/bin/env python3

import wx
import sys
import os

if (len(sys.argv) > 1 and sys.argv[1].startswith('-d')):
    from lib import GuideMonitor
else:
    from GuideMonitor import GuideMonitor

os.environ["EPICS_CA_ADDR_LIST"] = "10.2.2.107"
#os.environ["EPICS_CA_ADDR_LIST"] = "172.17.2.35"


if __name__ == '__main__':
    app = wx.App()
    GuideMonitor().Show(True)
    app.MainLoop()

