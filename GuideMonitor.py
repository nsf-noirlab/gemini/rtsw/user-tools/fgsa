#!/usr/bin/python 
"""
Gemini GuideMonitor application
"""
import os
import time
import sys
from numpy import array, where
import collections

#import matplotlib as mpl
import wx
import wx.lib.colourselect  as csel

from epics import PV
from epics.wx import EpicsFunction, DelayedEpicsCallback
from epics.wx.utils import  SimpleText, Closure, FloatCtrl

import numpy as np
from wxmplot.plotpanel import PlotPanel
from wxmplot.colors import hexcolor
#from wxmplot.utils import LabelEntry

from FGSAChannelList import FGSAChannelList
BG = (0,0,0)
WBG = (255,255,255)

FILECHARS = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_'


MENU_EXIT   = wx.NewId()
MENU_SAVE_IMG = wx.NewId()
MENU_SAVE_DAT = wx.NewId()
MENU_CONFIG = wx.NewId()
MENU_UNZOOM = wx.NewId()
MENU_HELP   = wx.NewId()
MENU_ABOUT  = wx.NewId()
MENU_PRINT  = wx.NewId()
MENU_PSETUP = wx.NewId()
MENU_PREVIEW = wx.NewId()
MENU_CLIPB  = wx.NewId()
MENU_SELECT_COLOR = wx.NewId()
MENU_SELECT_SMOOTH = wx.NewId()



class GuideMonitor(wx.Frame):

    default_colors = ((0, 0, 0), (0, 0, 255), (255, 0, 0),
                      (0, 0, 0), (255, 0, 255), (0, 125, 0))
    
    __version__ = ('1.0')
    def __init__(self, parent=None):
        self.pvdata = {}
        self.cid = 0
        #self.pvlist = ["m2:xTipPosHS", "m2:yTiltPosHS", "m2:zPosHS",
        #                "m2:xTipNetGuideHS", "m2:yTiltNetGuideHS", "m2:zNetGuideHS",
        #                "m2:vtkXCommand", "m2:vtkXFrequency", "m2:vtkXPhase",
        #                "m2:vtkYCommand", "m2:vtkYFrequency", "m2:vtkYPhase"]
        self.setChannels()
        self.pv = None #EPICS PV object
        self.simulate = False
        #self.pvactive = None
        self.channelChanged = True
        self.oldobj = None
        self.create_frame(parent)

        self.newdata = False
        self.plot_drawn = False
        self.updatecount = 0
        #if self.simulate is True:
        #    self.timer = wx.Timer(self)
        #    self.Bind(wx.EVT_TIMER, self.onUpdatePlot, self.timer) 
        #    self.timer.Start(100)
        self.ph = PlotHelper()

    def setChannels(self):
        
        self.cl = FGSAChannelList()
        cl = self.cl
        cl.addChannel('m2:xTipPosHS', 'X-Tip [arcsec]', 'M2 X-axis Position (Elevation)')
        cl.addChannel('m2:yTiltPosHS', 'Y-Tilt [arcsec]', 'M2 Y-axis Position (Azimuth)')
        cl.addChannel('m2:zPosHS', 'Z Focus [micron]', 'M2 Z-axis Position (Focus)')
        cl.addChannel('m2:xTipNetGuideHS', 'X-Guide [arcsec]', 'X Applied Guide (Elevation)')
        cl.addChannel('m2:yTiltNetGuideHS', 'Y-Guide [arcsec]', 'Y Applied Guide (Azimuth)')
        cl.addChannel('m2:zNetGuideHS', 'Z-Guide [micron]', 'Z Applied Guide (Focus)')
        cl.addChannel('m2:vtkXCommand', 'VTK-Xcmd [arcsec]', 'VTK X-Command (Elevation)')
        cl.addChannel('m2:vtkXFrequency', 'VTK-XFrequency [Hz]', 'VTK X-Frequency (Elevation)')
        cl.addChannel('m2:vtkXPhase', 'VTK-XPhase [degrees]', 'VTK X-Phase (Elevation)')
        cl.addChannel('m2:vtkYCommand', 'VTK-Ycmd [arcsec]', 'VTK Y-Command (Azimuth)')
        cl.addChannel('m2:vtkYFrequency', 'VTK-YFrequency [Hz]', 'VTK Y-Frequency (Azimuth)')
        cl.addChannel('m2:vtkYPhase', 'VTK-YPhase [degrees]', 'VTK Y-Phase (Azimuth)')
        cl.addChannel('m2:xRawGuideHS', 'XRawGuide [arcsec]', 'X Raw Guide (Elevation)')
        cl.addChannel('m2:yRawGuideHS', 'YRawGuide [arcsec]', 'Y Raw Guide (Azimuth)')
        cl.addChannel('m2:zRawGuideHS', 'ZRawGuide [micron]', 'Z Raw Guide (Focus)')


    def create_frame(self, parent, pos=(10,10),size=(650,1240), **kwds):
        self.parent = parent
        
        kwds['style'] = wx.DEFAULT_FRAME_STYLE
        kwds['size'] = size
        kwds['pos'] = pos
        wx.Frame.__init__(self, parent, -1, 'Gemini GuideMonitor', **kwds)
 
        self.plotpanel1 = PlotPanel(self, size=(460,360),fontsize=(7),trace_color_callback=None)
        #self.plotpanel1.messenger = self.write_message

        self.plotpanel2 = PlotPanel(self, size=(460,360),fontsize=(7), trace_color_callback=None)
        #self.plotpanel2.messenger = self.write_message

        self.build_btnpanel() 
        self.build_menus()

        self.SetBackgroundColour(wx.Colour(*WBG))
        mainsizer = wx.BoxSizer(wx.VERTICAL)

        # Add a panel so it looks the correct on all platforms
        p1 = wx.Panel(self)
        p1.SetBackgroundColour(wx.Colour(*WBG))

        #mainsizer.Add((1,1), proportion=0)
        mainsizer.Add(self.btnpanel, 0, wx.GROW|wx.EXPAND, 5)
        mainsizer.Add(self.plotpanel1, 1, wx.EXPAND, 5)
        mainsizer.Add(self.plotpanel2, 1, wx.EXPAND, 5)
        
        self.SetAutoLayout(True)
        self.SetSizerAndFit(mainsizer)
        #self.Fit()

        self.Refresh()

        #mypvname = 'm2:xTipPosHS'
        #mypvname = 'm2:yTiltPosHS'
        #mypvname = 'm2:zPosHS'
        #mypvname = 'm2:xTipNetGuideHS'

        #self.pvlist.append(mypvname)
        print(self.cl.names)
        #if not self.simulate:
            #self.connectChannel(self.pvlist[1])
            #self.connectChannel(self.cl.channelAt(0))


    def build_btnpanel(self):
        panel = self.btnpanel = wx.Panel(self, )
        panel.SetBackgroundColour(wx.Colour(*WBG))

        topsizer = wx.BoxSizer(wx.VERTICAL)
        btnsizer1 = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer2 = wx.BoxSizer(wx.HORIZONTAL)
        btnsizer3 = wx.BoxSizer(wx.HORIZONTAL)
        self.xpos_btn  = wx.ToggleButton(panel, label='X-Tip',  id=0, size=(100, 30))
        self.ypos_btn = wx.ToggleButton(panel, label='Y-Tip', id=1, size=(100, 30))
        self.zpos_btn = wx.ToggleButton(panel, label='Z-Tip', id=2, size=(100, 30))
        self.xguide_btn = wx.ToggleButton(panel, label='X-Guide', id=3, size=(100, 30))
        self.yguide_btn = wx.ToggleButton(panel, label='Y-Guide', id=4, size=(100, 30))
        self.zguide_btn = wx.ToggleButton(panel, label='Z-Guide', id=5, size=(100, 30))

        self.xvtkCmd_btn = wx.ToggleButton(panel, label='X-VTKcmd', id=6, size=(100, 30))
        self.xvtkFreq_btn = wx.ToggleButton(panel, label='X-VTKFrequency', id=7, size=(100, 30))
        self.xvtkPhase_btn = wx.ToggleButton(panel, label='X-VTKPhase', id=8, size=(100, 30))
        self.yvtkCmd_btn = wx.ToggleButton(panel, label='Y-VTKcmd', id=9, size=(100, 30))
        self.yvtkFreq_btn = wx.ToggleButton(panel, label='Y-VTKFrequency', id=10, size=(100, 30))
        self.yvtkPhase_btn = wx.ToggleButton(panel, label='Y-VTKPhase', id=11, size=(100, 30))
        self.xrawguide_btn = wx.ToggleButton(panel, label='X-RawGuide', id=12, size=(100, 30))
        self.yrawguide_btn = wx.ToggleButton(panel, label='Y-RawGuide', id=13, size=(100, 30))
        self.zrawguide_btn = wx.ToggleButton(panel, label='Z-RawGuide', id=14, size=(100, 30))
        self.xetfe_btn = wx.ToggleButton(panel, label='X-ETFE', id=15, size=(100, 30))
        self.yetfe_btn = wx.ToggleButton(panel, label='Y-ETFE', id=16, size=(100, 30))
        self.zetfe_btn = wx.ToggleButton(panel, label='Z-ETFE', id=17, size=(100, 30))

        self.xpos_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.ypos_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.zpos_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.xguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.zguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.xrawguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yrawguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.zrawguide_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.xetfe_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yetfe_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.zetfe_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)

        #self.xpos_btn.Enable()
        #self.xpos_btn.SetValue(True)
        self.xvtkCmd_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.xvtkFreq_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.xvtkPhase_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        #self.yvtkFreq_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yvtkCmd_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yvtkFreq_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)
        self.yvtkPhase_btn.Bind(wx.EVT_TOGGLEBUTTON, self.onPVModeChange)

        btnsizer1.Add(self.xpos_btn,  0, wx.CENTER, 2)
        btnsizer1.Add(self.ypos_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer1.Add(self.zpos_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer1.Add(self.xguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer1.Add(self.yguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer1.Add(self.zguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        
        btnsizer2.Add(self.xvtkCmd_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer2.Add(self.xvtkFreq_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer2.Add(self.xvtkPhase_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer2.Add(self.yvtkCmd_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer2.Add(self.yvtkFreq_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer2.Add(self.yvtkPhase_btn,  0, wx.RIGHT|wx.CENTER, 2)

        btnsizer3.Add(self.xrawguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer3.Add(self.yrawguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer3.Add(self.zrawguide_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer3.Add(self.xetfe_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer3.Add(self.yetfe_btn,  0, wx.RIGHT|wx.CENTER, 2)
        btnsizer3.Add(self.zetfe_btn,  0, wx.RIGHT|wx.CENTER, 2)

        topsizer.Add(btnsizer1, 1, wx.CENTER, 5)
        topsizer.Add(btnsizer2, 1, wx.CENTER, 5)
        topsizer.Add(btnsizer3, 0, wx.CENTER, 5)
        #topsizer.Add((1,1), wx.CENTER_HORIZONTAL)
        #topsizer.Add((-1,-1), wx.CENTER)
        panel.SetAutoLayout(True)
        panel.SetSizer(topsizer)
        topsizer.Fit(panel)

    def build_menus(self):
        mbar = wx.MenuBar()

        mfile = wx.Menu()
        mfile.Append(MENU_SAVE_DAT, "&Save Data\tCtrl+S",
                     "Save PNG Image of Plot")
        mfile.Append(MENU_SAVE_IMG, "Save Plot Image\t",
                     "Save PNG Image of Plot")
        mfile.Append(MENU_CLIPB, "&Copy Image to Clipboard\tCtrl+C",
                     "Copy Plot Image to Clipboard")
        mfile.AppendSeparator()
        mfile.Append(MENU_PSETUP, 'Page Setup...', 'Printer Setup')
        mfile.Append(MENU_PREVIEW, 'Print Preview...', 'Print Preview')
        mfile.Append(MENU_PRINT, "&Print\tCtrl+P", "Print Plot")
        mfile.AppendSeparator()
        mfile.Append(MENU_EXIT, "E&xit\tCtrl+Q", "Exit the 2D Plot Window")

        mopt = wx.Menu()
        mopt.Append(MENU_CONFIG, "Configure Plot\tCtrl+K",
                 "Configure Plot styles, colors, labels, etc")
        mopt.AppendSeparator()
        mopt.Append(MENU_UNZOOM, "Zoom Out\tCtrl+Z",
                 "Zoom out to full data range")

        mhelp = wx.Menu()
        mhelp.Append(MENU_HELP, "Quick Reference",  "Quick Reference for MPlot")
        mhelp.Append(MENU_ABOUT, "About", "About MPlot")

        mbar.Append(mfile, "File")
        mbar.Append(mopt, "Options")
        mbar.Append(mhelp, "&Help")

        self.SetMenuBar(mbar)
        self.Bind(wx.EVT_MENU, self.onSaveData, id=MENU_SAVE_DAT)
        self.Bind(wx.EVT_MENU, self.onHelp,     id=MENU_HELP)
        self.Bind(wx.EVT_MENU, self.onAbout,    id=MENU_ABOUT)
        self.Bind(wx.EVT_MENU, self.onExit,     id=MENU_EXIT)
        self.Bind(wx.EVT_CLOSE, self.onExit)

    def onPVModeChange(self, event=None):
        newobj = event.GetEventObject()
        if self.oldobj is None:
            self.oldobj = newobj
            print("ignored...disabled oldobj")
            #self.oldobj.SetValue(True)
        
        thischannel = self.cl.channelAt(newobj.GetId())
        #print 'thischannel is %s' % thischannel

        # 
        # Connect channel if togglebutton is pressed (True) 
        # and thischannel is not yet connected. 
        if (newobj.GetValue() is True) and (not thischannel.connected):
            self.channelChanged = True
            self.connectChannel(thischannel) 
        else:
            self.disConnectChannel(thischannel)
            
        self.oldobj = newobj

    @EpicsFunction
    def disConnectChannel(self, channel):
        if not channel.connected:
            return
        print("disconnecting %s\n" % channel.name)
        self.pv.disconnect()
        self.cl.setDisconnected(channel)
        if not self.cl.getConnectedList():
            self.newdata = False


    @EpicsFunction
    def connectChannel(self, channel=None):

        if channel is None:
            cname = str(self.cl.nameAt(0))
            #self.cid = 0
        else:
            #self.cid = channel.name
            cname = channel.name

        print('connect newchannel %s, label=%s' % (cname, channel.ylabel ))
        #X-Position
        pv = self.pv = PV(cname, callback=self.onChanges, auto_monitor = True)
        pv.get()
   
        #eventID = ca.create_subscription(chid, callback=self.onChanges)
        if pv is not None:
            if not pv.connected:
                pv.wait_for_connection()
            conn = pv.connected

            if conn:
                #msg = 'PV found: %s\n' % cname
                channel.connected = True
            else:
                msg = 'PV not found: %s' % cname
                sys.stdout.write(msg)
            self.pvdata[cname] = [(time.time(), pv.get())]
            self.cl.setConnected(channel)

    @DelayedEpicsCallback
    # define a callback function.  Note that this should
    # expect certain keyword arguments, including 'pvname' and 'value'
    def onChanges(self, pvname=None, value=None, **kw):
        #fmt = 'New Value: %s  value=%s, kw=%s\n'
        #sys.stdout.write(fmt % (pvname, str(value), repr(kw)))
        #sys.stdout.flush()
        #fmt = 'New Value: %d\n'
        #sys.stdout.write(fmt % ( len(value) ))
        #sys.stdout.flush()
        #ydat = array([i[0] for i in value])
        #tnow = time.time()
        #tdat = 0.005 * (array([i[0] for i in value]) + tnow)
        #ts = time.time()
        
        self.pvdata[pvname] = [(time.time(),array(value))]
        self.newdata = True
        c = self.cl.channelAt(self.cl.searchFirstOccurrence(pvname))
        if not self.cl.getConnectedList():
            return #empty list

        self.onUpdatePlot(c)

    def onToggle(self, event):
        btnLabel = self.toggleBtn.GetLabel()
        if btnLabel == "Start":
            print("starting timer...")
            self.timer.Start(1000)
            self.toggleBtn.SetLabel("Stop")
        else:
            print("timer stopped!")
            self.timer.Stop()
            self.toggleBtn.SetLabel("Start")
 
    def onUpdatePlot(self, channel, event=None):
  
        update_failed = False
        did_update = False
        tnow = time.localtime()
        timeplotdata = {} 
        timeplotlimits = {} 
        magplotdata = {} 
        magplotlimits = {} 
        freqs = {}
        if self.simulate:
            noise = np.random.normal
            srate = 200.0 
            t  = np.arange(0,20,1/srate)
            #y1 = np.sin(2*np.pi*12.1*t)
            y1 = 0.1*np.sin(2*np.pi*12.1*t) + noise(size=len(t), scale=0.01)
            #y1 = np.sin(x/3.4)/(0.2*x+2) + noise(size=n, scale=0.1)
            #y2 = 92 + 65*np.cos(x/16.) * np.exp(-x*x/7e3) + noise(size=n, scale=0.3)
            
            self.pvdata['simulate'] = [(tnow,array(y1))]
            self.newdata = True


        if self.updatecount == None:
            self.updatecount = 0

        if self.newdata == True:
            currentidx = self.cl.searchFirstOccurrence(channel.name)
            #iterate through all connect channel data
            for idx, name in enumerate(self.cl.getConnectedList()):
                #print idx, name
                timeplotdata[idx] = self.pvdata[name][0][1]
                timeplotlimits[idx] = self.ph.getPlotLimits(timeplotdata[idx],0)
                #print timeplotlimits[idx]
                freqs[idx], magplotdata[idx] = self.ph.getMagnitudeData(timeplotdata[idx])
                magplotlimits[idx] = self.ph.getPlotLimits(magplotdata[idx],1)
                #print magplotlimits[idx]
            
            #print "%s,%d,%d" % (self.pvdata[channel.name], currentidx, idx)
            #print "len ydat is %s" % len(ydat)
            tdat = np.linspace(-20, 0, len(timeplotdata[idx]) )
            #print 'ydat length=%d' % len(ydat)
            
            pp1 = self.plotpanel1
            pp2 = self.plotpanel2
            
            if (idx>1 and self.channelChanged):
                self.plot_drawn = False

            #Recalculate phase plot axes limits for autoscale
            #phaseplotxmin, phaseplotxmax = 0, 100.0
            #phaseplotymin, phaseplotymax = min(phase), max(phase)
            #span = abs(phaseplotymax-phaseplotymin)
            #limbuffer = 0.05*span
            #phaseplotymax = phaseplotymax+limbuffer
            #phaseplotymin = phaseplotymin-limbuffer

            thisYlabel = channel.ylabel #self.cl.ylabelAt(self.cid)
            thisTitle = channel.title #self.cl.titleAt(self.cid)
            #print thisYlabel
            if not self.plot_drawn:
                try:
                    pp1.plot(tdat, timeplotdata[idx], drawstyle='steps-post', xlabel='seconds',ylabel=thisYlabel,
                          title=thisTitle, linewidth=1)
                    if idx>1:
                        pp1.oplot(tdat, timeplotdata[idx-1], drawstyle='steps-post', linewidth=1)
                    pp2.plot(freqs[idx], magplotdata[idx], ylog_scale=True, ylabel='|FFT(%s)|'%thisYlabel,
                        #plot2(freqs, magnitude, ylog_scale=False, ylabel='|FFT(%s)|'%thisYlabel,
                        xlabel='frequency [Hz]', grid=True, linewidth=1,
                        title='%s %s' % (thisTitle, 'Magnitude Spectrum'),
                        #title='One-Sided Amplitude Spectrum',
                        xmin=0, xmax=100, ymin=magplotlimits[idx]['ymin'], ymax=magplotlimits[idx]['ymax'])
                        #autoscale=True)
                    #self.plotpanel2.oplot(freqs, phase, y2label='Phase',side='right', xmin=0,xmax=100,ymin=-400,ymax=400, color='red')
                    self.plot_drawn = True
                except:
                    update_failed = True
                    print("PLot failed...")

                #print phase
                #print("len(magnitude)=%d" % len(magnitude))
                #print("len(phase)=%d" % len(phase))
            else:
                '''Note below I'm sending one argument to set_xylims() by use of '()'. 
                   The argument is a new ordered list from the dictionary returned by getPlotLimits() above.
                   Why not just have getPlotLimits() return a tuple or list instead? 
                '''
                pp1.update_line(0, tdat, timeplotdata[idx], draw=False, update_limits=False)
                if idx>1:
                    pp1.update_line(1, tdat, timeplotdata[idx-1], draw=False, update_limits=False)
                pp1.set_xylims((timeplotlimits[idx]['xmin'], timeplotlimits[idx]['xmax'], timeplotlimits[idx]['ymin'], timeplotlimits[idx]['ymax']))

                pp2.update_line(0, freqs[idx], magplotdata[idx], draw=False, update_limits=False)
                #pp2.update_line(1, freqs, phase, draw=False, update_limits=False, side='right')
                pp2.set_xylims([magplotlimits[idx]['xmin'], magplotlimits[idx]['xmax'], magplotlimits[idx]['ymin'], magplotlimits[idx]['ymax']])
                #pp2.set_xylims((phaseplotxmin, 20, phaseplotymin, phaseplotymax), side='right')
                #pp2.update_line(0, freqs, ps, draw=False, update_limits=False)
                #pp2.update_line(0, freqs, magnitude, draw=False, update_limits=False)
                #pp2.set_xylims((0, 20, magplotymin, 0.05))

                if self.channelChanged:
                    pp1.set_ylabel(thisYlabel)
                    pp1.set_title(thisTitle)
                    pp2.set_ylabel('|FFT(%s)|'%thisYlabel)
                    pp2.set_title('%s %s'% (thisTitle, 'Magnitude Spectrum'))
                    #self.plotpanel2.set_title('One-Sided Amplitude Spectrum')
                    self.channelChanged = False

                did_update = True
            
            #left_axes.set_yscale('linear')

            if did_update:
                self.updatecount += 1
                #self.plotpanel1.canvas.draw()
                #self.plotpanel2.canvas.draw()

                if (self.updatecount == 20):
                    #self.plotpanel2.canvas.draw()
                    self.updatecount = 0
        else:
            self.newdata = False

        #self.plotpanel1.set_title(time.strftime("%Y-%b-%d %H:%M:%S", t))
        #self.plotpanel2.set_title(time.strftime("%Y-%b-%d %H:%M:%S", t))
         
        return
 

    def update(self, event):
        print("\nupdated: ", end=' ')
        print(time.ctime())


    def onSaveData(self, event=None):
        dlg = wx.FileDialog(self, message='Save Data to File...',
                            defaultDir = os.getcwd(),
                            defaultFile='PVStripChart.dat',
                            style=wx.SAVE|wx.CHANGE_DIR)
        if dlg.ShowModal() == wx.ID_OK:
            path = dlg.GetPath()
            self.SaveDataFiles(path)
            #self.write_message('Saved data to %s' % path)
            print('Saved data to %s' % path)
        dlg.Destroy()

    def SaveDataFiles(self, path):
        basename, ext = os.path.splitext(path)
        if len(ext) < 2:
            ext = '.dat'
        if ext.startswith('.'):
            ext = ext[1:]

        for pvname, data in list(self.pvdata.items()):
            print('SSS\n')
            print(data)
            print('SSS\n')
            return
            tnow = time.time()
            tmin = data[0][0]
            fname = []
            for s in pvname:
                if s not in FILECHARS:
                    s = '_'
                fname.append(s)
            fname = os.path.join("%s_%s.%s" % (basename, ''.join(fname), ext))

            buff = ["# Epics PV Strip Chart Data for PV: %s " % pvname]
            buff.append("# Current Time  = %s " % time.ctime(tnow))
            buff.append("# Earliest Time = %s " % time.ctime(tmin))
            buff.append("#------------------------------")
            buff.append("#  Timestamp         Value       Time-Current_Time(s)")
            for tx, yval in data:
                buff.append("  %.3f %16g     %.3f"  % (tx, yval, tx-tnow))

            fout = open(fname, 'w')
            fout.write("\n".join(buff))
            fout.close()
            #dat = tnow, func(tnow)

    def onAbout(self, event=None):
        dlg = wx.MessageDialog(self, self.about_msg,
                               "About Epics PV Strip Chart",
                               wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def onHelp(self, event=None):
        dlg = wx.MessageDialog(self, self.help_msg, "Epics PV Strip Chart Help",
                               wx.OK | wx.ICON_INFORMATION)
        dlg.ShowModal()
        dlg.Destroy()

    def onExit(self, event=None):
        try:
            self.plotpanel.win_config.Close(True)
            self.plotpanel.win_config.Destroy()
        except:
            pass

        self.Destroy()

class PlotHelper:

    
    def __init__(self):
        self.limitsprevious = {}
        self.limitscurrent = {}


    def getMagnitudeData(self, inputData):
        #magnitude = self.magnitudedata
        #time_step = 1.0/200
        #ps = 20*np.log10(np.abs(np.fft.rfft(ydat)))
        #Yfft = np.fft.fft(ydat*np.hamming( len(ydat)),4096)/4000
        #Yfft = np.fft.fft(ydat*np.hanning(len(ydat)),4096)/4000
        Yfft = np.fft.fft(inputData*np.hanning(len(inputData)),4000)/4000
        #magnitude = 2*np.abs(Yfft[0 : 4096/2+1]) #amplitude spectrum
        magnitude = 2*np.abs(Yfft[0 : 4000/2+1]) #amplitude spectrum
        power = magnitude**2
        #phase = np.unwrap(np.angle(Yfft[0 : 4000/2+1]))

        #freqs = np.fft.fftfreq(len(ydat), time_step)
        #idx = np.argsort(freqs)
        #plt.plot(freqs[idx], ps[idx])
        freqs = np.linspace(0, 100, len(magnitude))
        freqs = freqs[10 :]
        magnitude = magnitude[10 :]
        #phase = phase[10 :]
        #print magnitude 
        return (freqs, magnitude)

    def getPlotLimits(self, inputData, id=0):
        #Recalculate time plot axes limits for autoscale
        limits = {}
        #print("id=%d, lenth=%s" % (id, len(inputData)) )
        if (id == 0):
            #timeplotdata
            xmin, xmax = -20, 0.0
        else:
            #magplotdata
            xmin, xmax = 0, 100.0
        limits['xmin'] = xmin
        limits['xmax'] = xmax
        #print inputData
        ymin = min(inputData)
        ymax = max(inputData)
        span = abs(ymax-ymin)
        limbuffer = 0.05*span
        limits['ymin'] = ymin-limbuffer
        limits['ymax'] = ymax+limbuffer
        orderedlimits = collections.OrderedDict([("xmin",limits['xmin']), ("xmax",limits['xmax']), ("ymin",limits['ymin']), ("ymax",limits['ymax'])] )
        #return (xmin, xmax, ymin, ymax)
        names = ['key','data']
        formats = ['a4','f8']
        dtype = dict(names = names, formats=formats)
        array = np.array(list(orderedlimits.items()), dtype=dtype)
        #print array
        return orderedlimits


# Run the program
if __name__ == "__main__":
    app = wx.PySimpleApp()
    f = GuideMonitor()
    f.Show(True)
    app.MainLoop()
