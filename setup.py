from setuptools import setup
setup(name='fgsa',
        version='1.0',
        description='Gemini Fast Guide Spectrum Analyzer',
        author='Matt Rippa',
        author_email='mrippa@gemini.edu',
        py_modules=['fgsa', 'FGSAChannelList', 'GuideMonitor'],
        )
