#!/usr/bin/env python

import os
import sys

if not hasattr(sys, 'frozen'):
    import wxversion
    wxversion.ensureMinimal('2.8')

import wx
import numpy as np

from wxmplot.plotpanel import PlotPanel

import time
WBG = (255,255,255)
POLLTIME = 1000

class DataViewer(wx.Frame):

    def __init__(self, parent=None):

        self.create_frame(parent)
        dt = np.dtype([('timestamp', '<f7'), ('data', '<f4', (4000,))])
        self.dataframe = np.fromfile('test3.dat',dt)
        self.framenumber = 1

        print "dataset length=%d" % len(self.dataframe)
        print "dataframe length=%d" % len(self.dataframe[1][1])
        print "dataframe length=%d" % len(self.dataframe[2][1])
        return
        self.timer = wx.Timer(self)
        self.Bind(wx.EVT_TIMER, self.onUpdatePlot, self.timer)
        self.timer.Start(POLLTIME)


    def create_frame(self, parent, pos=(10,10),size=(650,650), **kwds):
        self.parent = parent
        
        kwds['style'] = wx.DEFAULT_FRAME_STYLE
        kwds['size'] = size
        kwds['pos'] = pos
        wx.Frame.__init__(self, parent, -1, 'Gemini GuideMonitor', **kwds)
 
        self.plotpanel1 = PlotPanel(self, size=(600,600),fontsize=(7),trace_color_callback=None)

        p1 = wx.Panel(self)
        p1.SetBackgroundColour(wx.Colour(*WBG))
        self.SetAutoLayout(True)
        self.Refresh()

    def onUpdatePlot(self, event=None):
        #if self.paused or not self.needs_refresh:
        #    return

        y = self.dataframe[self.framenumber][1]
        tdat = np.linspace(-20, 0, len(y))
        
        testdata = open("./t1.txt", 'a', 0)
        if not testdata:
            print ("Error open file for writing. Exit...")
            return
        np.set_printoptions(threshold='nan')
        print >> testdata, y

        #pframe.plot(x, y1, title='Test 2 Axes with different y scales',
              #xlabel='x (mm)', ylabel='y1', ymin=-0.75, ymax=0.75)
        #try:
        self.plotpanel1.plot(tdat, y, grid='True', drawstyle='steps-post', update_limits=True)
        #except:
        #    print "Plot failed"

        self.framenumber += 1

# Run the program
if __name__ == "__main__":
    app = wx.PySimpleApp()
    f = DataViewer()
    f.Show(True)
    app.MainLoop()
