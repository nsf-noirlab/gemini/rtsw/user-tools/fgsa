class FGSAChannel:

    def __init__(self, name, ylabel, title, connected):
        self.name = name
        self.ylabel = ylabel
        self.title = title
        self.connected = connected

    def elements(self):
        return list( (self.name, self.ylabel, self.title, self.connected) )

    def __repr__(self):
        return "[%s, %s, %s, %s] " % (self.name, self.ylabel, self.title, self.connected)
    
    def __str__(self):
        return 'c={0}, ylabel={1}, title={2}, connected={3}'.format(self.name, self.ylabel, self.title, self.connected)



class FGSAChannelList:
    
    channellist = {}

    def __init__(self, debug=False):
        #self.names = []
        #self.ylabels  = []
        #self.titles  = []
        self.debug = debug
        self.keys = []
        self.channels = []
        self.connectedlist = []

    def addChannel(self, name, ylabel=None, title=None, connected=False):
        self.keys.append(len(self.keys));
        channel = FGSAChannel(name, ylabel, title, connected)
        #self.names.append(name)
        #self.ylabels.append(ylabel)
        #self.titles.append(title)

        #self.channels.append([len(self.keys)-1, channel])
        self.channels.append(channel)
        self.channellist = dict(list(zip(self.keys, self.channels)))

        if connected is True:
            self.connectedlist.append(name)
        #print self.channellist

    def getConnectedList(self):
        return self.connectedlist

    #The inputA could be a Channel object or the name as string.
    def setConnected(self, inputA):
        if isinstance(inputA, FGSAChannel):
            inputA.connected = True
            self.connectedlist.append(inputA.name)
        elif (isinstance(inputA, str)):
            c = self.channelAt(self.searchFirstOccurrence(inputA))
            c.connected = True
            self.connectedlist.append(inputA)

    def setDisconnected(self, inputA):
        if isinstance(inputA, FGSAChannel):
            inputA.connected = False
            self.connectedlist.remove(inputA.name)
        elif (isinstance(inputA, str)):
            c = self.channelAt(self.searchFirstOccurrence(inputA))
            c.connected = False
            self.connectedlist.remove(name)
    
    def isConnected(self, name):
        return name in self.activelist

    def names(self):
        return self.names()

    def ylabels(self):
        return self.ylables()

    def titles(self):
        return self.titles()

    def channelAt(self, index):
        return self.channellist.get(index)

    def chidAt(self, channelIndex):
        c = self.channelAt(channelIndex)
        return c 
        #return c[0] #Channel ID

    def nameAt(self, channelIndex):
        return self.channelAt(channelIndex).name

    def ylabelAt(self, channelIndex):
        return self.channelAt(channelIndex).ylabel

    def titleAt(self, channelIndex):
        return self.channelAt(channelIndex).title 
    
    def showlist(self):
        for key in list(self.channellist.keys()):
            print("key: %s , value: %s" % (key, self.channellist[key]))

    def searchFirstOccurrence(self, searchFor):
        for k in list(self.channellist.keys()):
            for v in self.channellist[k].elements():
                if self.debug:
                    print("k:%s, searchFor: %s, v: %s" %(k, searchFor, v)) 
                if searchFor in str(v):
                    return k
                elif self.debug:
                    print("...fail")
            if self.debug:
                print("")
        return None


if __name__ == '__main__':

    cl = FGSAChannelList()
    cl.addChannel("m2:xTipPosHS", "X-Tip [arcsec]", "M2TS Azimuth")
    cl.addChannel("m2:yTiltPosHS", "Y-Tilt [arcsec]", "M2TS Elevation")
    cl.addChannel("m2:zPosHS", "Z Focus [micron]", "M2TS Focus")
    cl.addChannel('m2:xTipNetGuideHS', 'X-Guide [arcsec]', 'X Applied Guide (Elevation)')

    print("Channels:  %s " % repr(cl.channellist))
    print("Channel Keys:  %s " % list(cl.channellist.keys()))
    print("Channel Values:  %s " % list(cl.channellist.values()))
    #print "Channel Names:  %s " % repr(cl.names)
    #
    print("Channel Name At 1:  %s " % cl.nameAt(1))
    print("Channel At 0:  %s " % repr(cl.channelAt(0)))
    print("Channel Ylabel At 0:  %s " % cl.ylabelAt(0))
    print("Channel Ylabel At 1:  %s " % cl.ylabelAt(1))
    print("Channel Ylabel At 2:  %s " % cl.ylabelAt(2))
    #print "Channel Title At 0:  %s " % cl.titleAt(0)
    #print "Channel ID At 3:  %s " % cl.chidAt(2)
    print("cl.keys() %s" % list(cl.channellist.keys()))
    print("cl.values() %s\n" % list(cl.channellist.values()))

    cl.setConnected(cl.nameAt(0))
    cl.setConnected(cl.channelAt(2))
    cl.setConnected(cl.channelAt(3))
    cl.showlist()
    
    print("List of Connected channels are: ")
    for idx, val in enumerate(cl.getConnectedList()):
        print(idx, val) 
    print("")

    #print "Channels..........:  %s " % cl.channellist

    cl.setDisconnected(cl.channelAt(2))
    cl.showlist()

    print("After disconnect channel 2...")
    for idx, val in enumerate(cl.getConnectedList()):
        print(idx, val) 
    print("")
    #for c in cl.channellist:
    #    for v in cl.channellist[c.values]:
    #        print v

    print("") 
    print("X-Tip is in item %s\n" % cl.searchFirstOccurrence('X-Tip'))
    print("X-Tip is in item %s\n" % cl.searchFirstOccurrence('xTip'))
    print("m2:zPosHS is in item %s\n" % cl.searchFirstOccurrence('m2:zPosHS'))
    print("Elevation is in item %s\n" % cl.searchFirstOccurrence('Elevation'))


    print("Channel with m2:zPosHS is %s" % cl.channelAt(cl.searchFirstOccurrence('m2:zPosHS')))
    print("Channel with m2:xTipPosHS is %s" % cl.channelAt(cl.searchFirstOccurrence('m2:xTipPosHS')))
    print("Channel with m2:yTiltPosHS is %s" % cl.channelAt(cl.searchFirstOccurrence('m2:yTiltPosHS')))





